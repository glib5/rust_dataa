use std::env::current_dir;
use std::fs::File;
use std::path::{Path, PathBuf};

use polars::prelude::*;

fn main() {
    let datapath = get_parent_folder()
        .join("src")
        .join("simple_data.csv");

    let lf = read_csv(&datapath);

    add_col_example(&lf);

    let mut df = analyze(&lf);
    println!("{:?}", &df);

    let csvpath = datapath.with_file_name("out.csv");
    write_csv(&csvpath, &mut df);
    
    let parquetpath = datapath.with_file_name("out.parquet");
    write_parquet(&parquetpath, &mut df);   
}

fn get_parent_folder() -> PathBuf {
    // returns a PathBuf representing the cwd
    let cwd_str = current_dir().unwrap()
        .as_os_str()
        .to_str().unwrap()
        .to_string();
    let parent_folder = Path::new(&cwd_str).to_path_buf();
    parent_folder
}

fn read_csv(path: &PathBuf) -> LazyFrame {
    // lazily read a csv file
    let lf = LazyCsvReader::new(&path)
        .has_header(true)
        .finish().unwrap();
    lf
}

fn write_csv(csvpath: &PathBuf, data: &mut DataFrame) -> () {
    // write a df to csv
    let csvfile = File::create(csvpath).unwrap();
    CsvWriter::new(&csvfile).finish(data).unwrap();
}

fn write_parquet(parquetpath: &PathBuf, data: &mut DataFrame) -> () {
    // write a df to parquet
    let csvfile = File::create(parquetpath).unwrap();
    ParquetWriter::new(&csvfile).finish(data).unwrap();
}

fn analyze(lf: &LazyFrame) -> DataFrame {
    // analyis here
    lf.clone()
    .filter(col("NAME").is_not_null())
    .groupby([col("NAME")])
    .agg([
        col("NAME").count().alias("NAME_COUNT"),
        col("AGE").min().alias("MIN_AGE"),
        col("GRADE").max().alias("MAX_GRADE")
    ])
    .sort_by_exprs(vec![col("NAME")], vec![false], false)
    .collect().unwrap()
}

fn add_col_example(lf: &LazyFrame) -> () {
    // how to add columns
    let df = lf.clone()
        .with_column((col("AGE") + col("GRADE")).alias("AGE+GRADE"))
        .collect().unwrap();
    println!("{:?}", &df);
}



